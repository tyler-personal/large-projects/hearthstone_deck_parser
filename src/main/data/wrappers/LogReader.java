package main.data.wrappers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogReader {
    private String file;
    private boolean threadOn = false;

    public LogReader(String file) { this.file = file; }

    public void startWithLineOperation(Consumer<String> operationForLine) {
        new Thread(() -> readLinesContinually(operationForLine)).start();
        threadOn = true;
    }

    public void stop() { threadOn = false; }

    private void readLinesContinually(Consumer<String> operationForLine) {
        try(var br = new BufferedReader(new FileReader(file))) {
            Stream.generate(br::lines)
                .map(linesStream -> linesStream.collect(Collectors.toList()))
                .filter(lines -> !lines.isEmpty())
                .takeWhile(lines -> threadOn)
                .forEach(lines -> lines.forEach(operationForLine));
        } catch(Exception e) {
            try {
                System.out.println("File doesn't exist. Rerunning in 5 seconds.");
                if(threadOn) {
                    Thread.sleep(5000);
                    readLinesContinually(operationForLine);
                }
            }
            catch(Exception e2) {
                System.out.println("EXCEPTION: LogReader#readLinesContinually#sleep");
            }
        }

    }
}
