package main.presentation.presenters;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import main.domain.core.models.Deck;
import main.domain.core.models.Archetype;
import main.presentation.controllers.MainController;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class MainPresenter {
    private Archetype archetype;
    private Deck deck;
    private int index = 0;

    private MainController controller;
    public MainPresenter(MainController controller) { this.controller = controller; }

    public void setNewArchetype(Archetype archetype) {
        this.archetype = archetype;
        controller.setDeck(archetype.decks.get(0));
    }

    public void nextDeck() { determineDeck(1); }
    public void previousDeck() { determineDeck(-1); }

    private void determineDeck(int indexChange) {
        if(indexInBounds()) index += indexChange;
        deck = archetype.decks.get(index);
        controller.setDeck(deck);
    }
    private boolean indexInBounds() { return index > 0 && index < archetype.decks.size(); }

    public String getDeckLabel() {
        return deck.name + " (variation: " + String.valueOf(index + 1) + ")";
    }

    public <T> void set(ObservableList<T> main, List<T> li) {
        main.clear();
        main.addAll(li);
    }

    public <T> void addTableListener (TableView<T> table, ChangeListener<T> listener) {
        table.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    public <T> void addColumnListener(TableView<T> table, Consumer<TableRow<T>> isSelected, Consumer<TableRow<T>> wasSelected) {
        table.setRowFactory(t -> {
            TableRow<T> row = new TableRow<>();
            row.selectedProperty().addListener((obs, mWasSelected, mIsSelected) -> {
                if(mIsSelected) isSelected.accept(row);
                if(mWasSelected) wasSelected.accept(row);
            });
            return row;
        });
    }

    public <T> void setupColumn(TableColumn<T, String> column, Function<T, String> getter) {
        System.out.println(column);
        System.out.println(getter);
        column.setCellValueFactory(data -> new SimpleStringProperty(getter.apply(data.getValue())));
    }

}