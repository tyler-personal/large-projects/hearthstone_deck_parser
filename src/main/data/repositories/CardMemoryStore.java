package main.data.repositories;

import main.data.repositories.boundaries.gateways.CardGateway;
import main.domain.core.models.Card;
import main.domain.boundaries.repositories.CardRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CardMemoryStore implements CardRepository {
    private Map<String, Card> nameOrIdToCard = new HashMap<>();

    public Card getOne(String nameOrID) { return nameOrIdToCard.get(nameOrID); }

    public CardMemoryStore(CardGateway gateway) {
        gateway.createAll().forEach(card -> {
            nameOrIdToCard.put(card.name, card);
            nameOrIdToCard.put(card.id, card);
        });
    }

}
