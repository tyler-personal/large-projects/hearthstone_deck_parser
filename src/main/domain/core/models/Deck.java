package main.domain.core.models;

import java.util.List;
import java.util.stream.Collectors;

public class Deck {
    private static int currentId = 0;

    public final int id;
    public final String name;
    public final String winRate;
    public final int gamesPlayedCount;
    public final int averageDurationInMinutes;
    public final String playerClass;
    public final String iconURL;

    public final List<Card> cards;
    public final List<String> cardNames;

    public Deck(
        String name, double winRate, int gamesPlayedCount, int averageDuration,
        String playerClass, String iconURL, List<Card> cards
    ) {
        this.id = currentId;
        this.name = name;
        this.winRate = String.valueOf(winRate);
        this.gamesPlayedCount = gamesPlayedCount;
        this.averageDurationInMinutes = averageDuration;

        this.playerClass = playerClass;
        this.iconURL = iconURL;
        this.cards = cards;
        this.cardNames = cards.stream().map(card -> card.name).collect(Collectors.toList());

        currentId++;
    }
}
