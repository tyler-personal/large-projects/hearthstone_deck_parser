package main.domain.boundaries.repositories;

import main.domain.core.models.Deck;

import java.util.List;

public interface DeckRepository {
    List<Deck> getAll();
    List<Deck> getAllByCardName(String cardName);
    Deck getOne(int deckID);
}
