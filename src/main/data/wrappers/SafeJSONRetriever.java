package main.data.wrappers;

import org.json.JSONObject;

public class SafeJSONRetriever {
    private JSONObject jsonObject;
    public SafeJSONRetriever(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getString(String key) {
        try {
            return (String) jsonObject.get(key);
        } catch(Exception e) {
            return "N.A";
        }
    }

    public int getInt(String key) {
        try {
            return (int) jsonObject.get(key);
        } catch(Exception e) {
            return 0;
        }
    }

    public double getDouble(String key) {
        try {
            return (double) jsonObject.get(key);
        } catch(Exception e) {
            return 0.0;
        }
    }
}
