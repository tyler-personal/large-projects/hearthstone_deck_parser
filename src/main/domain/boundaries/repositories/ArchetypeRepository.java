package main.domain.boundaries.repositories;

import main.domain.core.models.Archetype;

import java.util.List;

public interface ArchetypeRepository {
    List<Archetype> getAll();
}
