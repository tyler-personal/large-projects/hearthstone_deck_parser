package main.data.gateways;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import main.data.wrappers.SafeJSONRetriever;
import main.data.repositories.boundaries.gateways.DeckGateway;
import main.data.wrappers.BaseAPI;
import main.domain.core.models.Deck;
import main.domain.boundaries.repositories.CardRepository;
import org.json.JSONObject;
import tools.U;

public class ParseHub implements DeckGateway {
    private static final String URL = "https://parsehub.com/api/v2/";
    private static final String KEY = "tadgGWTxeJ-7";
    private final Project project;

    public ParseHub(Project project) {
        this.project = project;
    }

    public List<Deck> createAll(CardRepository cardRepository) {
        var url = URL + "projects/" + project.token + "/last_ready_run/data?api_key=" + KEY;
        return U.jsonArrayStreamOfJsonObjects(BaseAPI.getObject(url).getJSONArray("decks"))
            .map(jObj -> createDeckFromJson(jObj, cardRepository))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private Deck createDeckFromJson(JSONObject jObj, CardRepository cardRepository) {
        if (jObj.keySet().size() == 0)
            return null;

        var safeJObj = new SafeJSONRetriever(jObj);
        return new Deck(
            safeJObj.getString("name"),
            safeJObj.getDouble("win_rate"),
            safeJObj.getInt("games"),
            safeJObj.getInt("duration"),
            safeJObj.getString("player_class"),
            safeJObj.getString("icon_url"),
            U.jsonArrayStreamOfJsonObjects(jObj.getJSONArray("cards")).flatMap(j -> {
                var safeJ = new SafeJSONRetriever(j);
                var name = safeJ.getString("name");
                var count = safeJ.getInt("count");
                return Collections.nCopies(count, name).stream();
            }).map(cardRepository::getOne).collect(Collectors.toList())
        );
    }

    public enum Project {
        STANDARD("t0q_CqCT5OyX"),
        WILD("Not Available");

        String token;
        Project(final String token) { this.token = token; }
    }
}
