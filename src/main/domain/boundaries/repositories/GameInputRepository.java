package main.domain.boundaries.repositories;

import main.domain.core.models.Card;
import tools.Procedure;

import java.util.function.Consumer;

public interface GameInputRepository {
    void addCardListener(Consumer<Card> cardListener);
    void addEndGameProcedure(Procedure endGameListener);
    void startListening();
}
