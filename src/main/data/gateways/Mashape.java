package main.data.gateways;

import main.data.wrappers.SafeJSONRetriever;
import main.data.repositories.boundaries.gateways.CardGateway;
import main.data.wrappers.BaseAPI;
import main.domain.core.models.Card;
import org.json.JSONArray;
import org.json.JSONObject;
import tools.U;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Mashape implements CardGateway { // NOTE: Might want to instantiate this with Unirest key WAY down the road (not sure).
    private final static String URL = "https://omgvamp-hearthstone-v1.p.mashape.com/";
    private final static String KEY = "IqKTxuwPTdmsh1u0noggFz7oLHvjp1LystZjsncezRXsKdtjRf";
    private final static Map<String, String> HEADER = Map.of("X-Mashape-Key", KEY, "Accept", "application/json");

    JSONObject getCardJSON(String nameOrID) {
        var url = URL + "cards/" + nameOrID;
        return BaseAPI.getObject(url, HEADER);
    }

    public List<Card> createAll() {
        return getAllCardJSON().stream()
            .flatMap(U::jsonArrayStreamOfJsonObjects)
            .map(this::createCardFromJson)
            .collect(Collectors.toList());
    }

    private List<JSONArray> getAllCardJSON() {
        var jObj = (JSONObject) BaseAPI.getArray(URL + "cards", HEADER).get(0);
        return jObj.keySet().stream().map(key -> (JSONArray) jObj.get(key)).collect(Collectors.toList());
    }

    private Card createCardFromJson(JSONObject jObj) {
        var safeJObj = new SafeJSONRetriever(jObj);
        return new Card(
            safeJObj.getString("name"),
            safeJObj.getString("cardID"),
            safeJObj.getInt("cost"),
            cleanDescription(safeJObj.getString("description")),
            safeJObj.getString("playerClass"),
            safeJObj.getString("img")
        );
    }

    private String cleanDescription(String description) {
        return description
            .replace("<b>", "").replace("</b>", "").replace("[x]", "").replace("\\n", " ").replace("$", "")
            .replace("<i>", "").replace("</i>", "");
    }



}
