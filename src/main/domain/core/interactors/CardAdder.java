package main.domain.core.interactors;

import main.domain.core.models.Card;

public class CardAdder {
    public void perform(Card card) {
        cardsPlayed.add(card);

        if(cardsLeft.contains(card))
            cardsLeft.remove(card);
        else // TODO: Run calculation for deck existence (in interactor)
            gameDisplay.resetCurrentDeck();

        removeArchetypesWithoutDecks();

        System.out.println("Running!" + archetypes.size());
        gameDisplay.set(archetypes, cardsPlayed, cardsLeft);
    }
}
