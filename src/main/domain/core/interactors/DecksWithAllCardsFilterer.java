package main.domain.core.interactors;

import main.domain.core.models.Card;
import main.domain.core.models.Deck;

import java.util.List;
import java.util.stream.Collectors;

public class DecksWithAllCardsFilterer {
    public List<Deck> perform(List<Deck> decks, List<Card> cards) {
        var cardNames = cards.stream().map(card -> card.name).collect(Collectors.toList());
        return decks.stream()
            .filter(deck -> deck.cardNames.containsAll(cardNames))
            .collect(Collectors.toList());
    }
}
