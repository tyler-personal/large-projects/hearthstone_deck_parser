package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import main.data.repositories.boundaries.gateways.CardGateway;
import main.data.repositories.boundaries.gateways.DeckGateway;
import main.data.gateways.Mashape;
import main.data.gateways.ParseHub;
import main.data.repositories.ArchetypeMemoryStore;
import main.data.repositories.CardMemoryStore;
import main.data.repositories.DeckMemoryStore;
import main.data.repositories.PowerLogListener;
import main.domain.boundaries.repositories.ArchetypeRepository;
import main.domain.boundaries.repositories.CardRepository;
import main.domain.boundaries.repositories.DeckRepository;
import main.domain.boundaries.repositories.GameInputRepository;

import java.util.concurrent.atomic.AtomicReference;

/* TODO LIST:
 * Stop generated spells from counting against the opponents deck (initially coin).
 * Allow secrets to count towards generated spells.
 * Allow users to ignore certain cards.
 * When out of deck options, expand automatically.
 * Narrow deck options by class automatically.
 */
public class Main extends Application {
    // TODO: Look into dependency injection frameworks to replace this bootstrapping code.
    public static CardGateway cardGateway;
    public static DeckGateway deckGateway;

    public static CardRepository cardRepo;
    public static DeckRepository deckRepo;
    public static ArchetypeRepository archetypeRepo;
    public static GameInputRepository gameInputRepo;

    private void bootstrap() {
        cardGateway = new Mashape();
        deckGateway = new ParseHub(ParseHub.Project.STANDARD);

        cardRepo = new CardMemoryStore(cardGateway);
        deckRepo = new DeckMemoryStore(deckGateway, cardRepo);
        archetypeRepo = new ArchetypeMemoryStore(deckRepo);
        gameInputRepo = new PowerLogListener(cardRepo, showDialogForHearthstoneDIRAndHold());
    }

    private String showDialogForHearthstoneDIRAndHold() {
        var dialog = new TextInputDialog("C:\\Games\\Hearthstone");
        AtomicReference<String> result = new AtomicReference<>();

        dialog.setTitle("Enter Hearthstone Directory");
        dialog.setHeaderText("Enter Hearthstone directory, or use default below.");
        dialog.showAndWait().ifPresentOrElse(
            val -> result.set(val + "\\Logs\\Power.log"),
            () -> result.set("C:\\Games\\Hearthstone\\Logs\\Power.log")
        );
        return result.get();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        bootstrap();
        Parent root = FXMLLoader.load(getClass().getResource("presentation/views/main.fxml"));
        var scene = new Scene(root);

        primaryStage.setTitle("Hearthstone Deck Parser");
        primaryStage.setScene(scene);
        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}
