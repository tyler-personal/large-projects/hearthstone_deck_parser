package main.presentation.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import main.Main;
import main.domain.core.entities.GameState;
import main.domain.core.models.Archetype;
import main.domain.core.models.Card;
import main.domain.core.models.Deck;
import main.domain.boundaries.displays.GameDisplay;
import main.presentation.presenters.MainPresenter;

import java.util.ArrayList;
import java.util.List;

public class MainController implements GameDisplay {
    @FXML private TableView<Archetype> decksTable;
    @FXML private TableColumn<Archetype, String> decksColumn;
    @FXML private Button previousVariationButton;
    @FXML private Button nextVariationButton;

    @FXML private Label deckNameLabel;
    @FXML private Label winRateLabel;
    @FXML private Label gamesPlayedLabel;
    @FXML private Label gameDurationLabel;

    @FXML private TableView<Card> cardsTable;
    @FXML private TableColumn<Card, String> nameColumn;
    @FXML private TableColumn<Card, String> costColumn;
    @FXML private TableColumn<Card, String> descriptionColumn;

    @FXML private TableView<Card> enemyCardsTable;
    @FXML private TableColumn<Card, String> enemyCardsColumn;

    private MainPresenter presenter;

    private ObservableList<Archetype> archetypes = FXCollections.observableList(new ArrayList<>());
    private ObservableList<Card> cardsPlayed = FXCollections.observableList(new ArrayList<>());
    private ObservableList<Card> cardsLeft = FXCollections.observableList(new ArrayList<>());

    @FXML private void initialize() {
        presenter = new MainPresenter(this);
        new GameState().start(this, Main.gameInputRepo);

        setupColumnWidths();
        setupTables();
        setupColumns();
        setupButtons();
    }

    public void set(List<Archetype> archetypes, List<Card> cardsPlayed, List<Card> cardsLeft) {
        presenter.set(this.archetypes, archetypes);
        presenter.set(this.cardsPlayed, cardsPlayed);
        presenter.set(this.cardsLeft, cardsLeft);
    }

    public void reset() {
        List.of(deckNameLabel, winRateLabel, gamesPlayedLabel, gameDurationLabel).forEach(l -> l.setText(""));
        cardsLeft.clear();
        archetypes.clear();
        cardsPlayed.clear();
    }

    public void resetCurrentDeck() {

    }

    private void setupTables() {
        decksTable.setItems(archetypes);
        enemyCardsTable.setItems(cardsPlayed);
        cardsTable.setItems(cardsLeft);

        var blackBackground = "-fx-background-color: black";
        presenter.addColumnListener(decksTable, row -> row.setStyle(blackBackground), row -> row.setStyle(""));
        presenter.addTableListener(decksTable, (observable, oldVal, newVal) -> presenter.setNewArchetype(newVal));
    }

    public void setDeck(Deck deck) {
        deckNameLabel.setText(presenter.getDeckLabel());
        winRateLabel.setText(deck.winRate);
        gamesPlayedLabel.setText(String.valueOf(deck.gamesPlayedCount));
        gameDurationLabel.setText(String.valueOf(deck.averageDurationInMinutes));

        presenter.set(cardsLeft, deck.cards);
        cardsLeft.removeAll(cardsPlayed);
    }

    private void setupColumns() {
        presenter.setupColumn(decksColumn, archetype -> archetype.name);
        presenter.setupColumn(enemyCardsColumn, card -> card.name);
        presenter.setupColumn(nameColumn, card -> card.name);
        presenter.setupColumn(costColumn, card -> String.valueOf(card.cost));
        presenter.setupColumn(descriptionColumn, card -> String.valueOf(card.cost));
    }

    private void setupColumnWidths() {
        var width = cardsTable.widthProperty().subtract(costColumn.getWidth()).divide(4);
        descriptionColumn.prefWidthProperty().bind(width.multiply(3));
        nameColumn.prefWidthProperty().bind(width);

        decksTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    private void setupButtons() {
        previousVariationButton.setOnAction(e -> presenter.previousDeck());
        nextVariationButton.setOnAction(e -> presenter.nextDeck());
    }
}
